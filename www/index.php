<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Создай свою сказку</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>

	<style>
	.create-btn{

	}
	.create-form{
		display:none;
	 }
	  </style>
    <!-- Full Page Image Header Area -->
    <div id="top" class="header">
        <div class="vert-text">
            <h1>Сегодня такой чудесный день</h1>
            <h3 class="create-btn">
                Разрешите рассказать Вам одну чудесную сказку?
			</h3>
			<div class="create-btn">
				<a href="#"  class="btn btn-default btn-lg" >Разрешить</a>
			</div>
			<div class="row" style="width:90%;padding-top:20px;">
				<form action="./new" class="create-form">
						<div class="col-md-3  col-md-offset-5 text-center" >
							<input name="user_link" class="form-control" placeholder="http://vk.com/igor.suvorov">

						</div>
						<div class="col-md-1 text-center">
							<input type="submit" class="btn btn-success btn-sm" value="Создать!">
						</div>

				</form>
			</div>
			<div class="row" style="width:90%;padding-top:20px;">
				<div class="col-md-4  col-md-offset-5 text-center" >
					<p>

					</p>
				</div>
			</div>
            <h3 class="create-form">
                Укажите пожалуйста ссылку на свою страницу VK<br/>
                или на страницу друга, для которого вы хотите создать сказку
            </h3>

        </div>
    </div>
    <!-- /Full Page Image Header Area -->


    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
	<script>
	$(function(){
		$('.create-btn a').click(function(){
			$('.create-btn').hide();
			$('.create-form').show();
			return false;
		});
	});
	</script>
    </script>
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter22194755 = new Ya.Metrika({id:22194755, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/22194755" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
</body>

</html>
