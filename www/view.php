<?php
	$file = __DIR__.'/tales/'.$_GET['id'].'.json';
	if(!is_file($file)){
		header('location:/tales');
		exit;
	}
	$users = json_decode(file_get_contents($file),1);



	function print_fairytale($users){
		?>
		
		<div class="row">
			<div class="col-md-4 text-center" style="padding-top:10px;"> 
				<img src="<?php echo $users[0]['photo_200']; ?>" class="img-circle" style="width:90%;">
				<?php echo $users[0]['first_name']; ?><br>
				<?php echo $users[0]['last_name']; ?>
			</div>	
			<div class="col-md-8 text-center"> 
				<div class="row" >
				<?php	
				foreach(array_slice($users, 1) as $user){
					?>
					<div class="col-md-4 text-center" style="padding:10px;"> 
						<img src="<?php echo $user['photo_200']; ?>" class="img-circle" style="width:75%;">
					</div>
					<?php		
				}
				?>
				</div>
				<div style="text-align:right;">
					<a href="<?php echo $users[0]['tale_link']; ?>">Прочитать сказку &#8594;</a>
				</div>
			</div>	
		</div>
		<hr>
		<?php		
	}
	
	function print_user($user){
		print_user_single($user);
	}
	
	function print_user_single($user){
		?>
		
			<div class="row">
				<div class="col-md-6  col-md-offset-3 text-center" > 
					<hr>
				</div>
				<div class="col-md-3  col-md-offset-2 text-center"> 
					<img src="<?php echo $user['photo_200']; ?>" class="img-circle">
				</div>
				<div class="col-md-5 text-center" > 
					<h2><?php echo $user['fairy_name']; ?> <?php echo $user['first_name']; ?></h2>
					<p class="lead"><?php echo $user['status']; ?></p>
				</div>
            </div>
		<?php
	}
	
	function print_user_part($user, $big = false){
		$name = $user['fairy_name'] . ' ' . $user['first_name'];
		if($big){
			$name = $user['first_name'] . ' ' . $user['last_name'];
		}
		?>
			<div class="row">
				<div class="col-md-4 text-center"> 
					<img src="<?php echo $user['photo_200']; ?>" class="img-circle" style="width:100%;">
				</div>
				<div class="col-md-8 text-center" > 
					<h2><?php echo $name; ?></h2>
					<p class="lead"><?php echo $user['status']; ?></p>
				</div>
            </div>
		<?php
	}
	
	
	function get_random_tales($limit = 5){
		$file = __DIR__.'/tales';
		$files = scandir($file);
		$files = array_slice($files, 2);
		shuffle($files);
		$tales = array();
		for($i = 0; $i < $limit; $i++){
			
			$data = file_get_contents($file . '/' . $files[$i]);
			$tale = json_decode($data, 1);
			$tale[0]['tale_link'] = './'.substr( $files[$i], 0, -5);
			$tales[] = $tale;
		}
		return $tales;
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Этот рассказ основан на реальных события. Его рассказал мне мой дед">
    <meta name="author" content="<?php echo $users[0]['first_name']; ?> <?php echo $users[0]['last_name']; ?>">
    <title><?php echo $users[0]['first_name']; ?> <?php echo $users[0]['last_name']; ?> хочет рассказать Вам сказку</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div id="newtop" class="header">
        <div class="vert-text">
            <h1><img src="<?php echo $users[0]['photo_200']; ?>" style="height:1px;"> <?php echo $users[0]['first_name']; ?> <?php echo $users[0]['last_name']; ?> хочет рассказать Вам сказку</h1>
        </div>
    </div>

    <div id="about" class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2>Посадил Дед репку и говорит:</h2>
					<p class="lead">
						Расти, расти, репка, сладкá! Расти, расти, репка, крепкá!
						Выросла репка сладкá, крепкá, большая-пребольшая.
					</p>
                </div>
            </div>
			<?php
				print_user($users[1]);
			?>
        </div>
    </div>

    <div class="callout spoiler" id="spoiler1">
        <div class="vert-text">
            <h1>Но не тут то было</h1>
        </div>
    </div>

    <!-- Services -->
    <div id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2>Выросла репка сладка, репка, большая-пребольшая</h2>
					<p class="lead">
						Пошел дед репку рвать:<br> тянет-потянетm<br> вытянуть не может
					</p>
                    <hr>
                    <h3><abbr title="Какого черта?">WTF?</abbr>, справедливо подметил <?php echo $users[1]['first_name']; ?></h3>
                </div>
            </div>
        </div>
    </div>
    <!-- /Services -->
    <!-- Intro -->
    <div id="about" class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2>Позвал дед бабку</h2>
					<p class="lead">
						Бабка за дедку<br>
						Дедка за репку
					</p>
                    <h3>Тянут-потянут, вытянуть не могут</h3>
                </div>
            </div>
			<?php print_user($users[2]); ?>
        </div>
    </div>
	
    <div id="about" class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2>Позвала бабка внучку</h2>
					<p class="lead">
						Внучка за бабку,<br>
						Бабка за дедку,<br>
						Дедка за репку 
					</p>
                    <h3>Тянут-потянут, вытянуть не могут</h3>
                </div>
            </div>
			<?php print_user($users[3]); ?>
        </div>
    </div>
	
    <!-- Callout -->
    <div  class="callout spoiler" id="spoiler2">
        <div class="vert-text">
            <h1>Сюжет предвещает быть ппц каким нелинейным</h1>
            <h3>подумала <?php echo $users[3]['first_name']; ?></h3>
        </div>
    </div>
    <!-- /Callout -->
	
    <div id="about" class="intro">
        <div class="container">
            <div class="row">
	
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2>Позвала внучка Пса</h2>
					<p class="lead">
						Пёс за внучку,<br>
						Внучка за бабку,<br>
						Бабка за дедку,<br>
						Дедка за репку 
					</p>
                    <h3>Тянут-потянут, вытянуть не могут</h3>
                </div>
            </div>
			<?php print_user($users[4]); ?>
        </div>
    </div>
	
		
    <div id="about" class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2>Позвал Пёс Котана</h2>
					<p class="lead">
                        Котан за Пса,<br>
						Пёс за внучку,<br>
						Внучка за бабку,<br>
						Бабка за дедку,<br>
						Дедка за репку<br>
					</p>
                    <h3>Тянут-потянут, вытянуть не могут</h3>
                </div>
            </div>
			<?php print_user($users[5]); ?>
        </div>
    </div>
    <!-- Callout -->
    <div  class="callout spoiler" id="spoiler3">
        <div class="vert-text">
            <h1>Вы не поверите, что произошло потом</h1>
            <h3>обмолвился <?php echo $users[5]['first_name']; ?></h3>
        </div>
    </div>
    <!-- /Callout -->
    <div id="about" class="intro">
        <div class="container">
            <div class="row">
	
                <div class="col-md-6  col-md-offset-3 text-center">
                    <h2>Позвал Котан Мышонка</h2>
					<p class="lead">
						Мышонок за Котана,<br>
						Котан за Пса,<br>
						Пёс за внучку,<br>
						Внучка за бабку,<br>
						Бабка за дедку,<br>
						Дедка за репку<br>
					</p>
                    <h3>Тянут-потянут — и вытянули репку</h3>
                </div>
            </div>
			<?php print_user($users[6]); ?>
        </div>
    </div>
	
	
    <!-- /Intro -->
	
    <!-- Callout -->
    <div  class="callout spoiler" id="spoiler4">
        <div class="vert-text">
            <h1>На этом и сказке конец</h1>
            <h3>Сказал свою заключительную реплику Мышонок <?php echo $users[1]['first_name']; ?></h3>
        </div>
    </div>
    <!-- /Callout -->
	<style>
	.starring > div{
		padding-bottom:30px;
	}
	</style>
	
	
    <!-- Portfolio -->
    <div id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <h2>В главных ролях</h2>
                    <hr>
                </div>
            </div>
			 <div class="row starring">
                <div class="col-md-6 text-center">
                    <?php print_user_part($users[1]); ?>
                </div>
                <div class="col-md-6 text-center">
                    <?php print_user_part($users[2]); ?>
                </div>
			</div>
			 <div class="row starring">
                <div class="col-md-6 text-center">
                    <?php print_user_part($users[3]); ?>
                </div>
                <div class="col-md-6 text-center">
                    <?php print_user_part($users[4]); ?>
                </div>
			</div>
			 <div class="row starring">
                <div class="col-md-6 text-center">
                    <?php print_user_part($users[5]); ?>
                </div>
                <div class="col-md-6 text-center">
                    <?php print_user_part($users[6]); ?>
                </div>
            </div>
			 <div class="row" style="padding-top:50px;">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <h2>Режиссер и продюссер</h2>
                    <hr>
                </div>
            </div>
			 <div class="row starring">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <?php print_user_part($users[0], 1); ?>
                </div>
            </div>
			
        </div>
    </div>
    <!-- /Portfolio -->

    <!-- Call to Action -->
    <div class="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2>Понравилась сказка?</h2>
                </div>
            </div>
			<div class="row">
                <div class="col-md-4 col-md-offset-2 text-center">
                    <h3>Расскажи о ней друзьям</h3>
                    <script type="text/javascript">(function() {
					  if (window.pluso)if (typeof window.pluso.start == "function") return;
					  if (window.ifpluso==undefined) { window.ifpluso = 1;
						var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
						s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
						s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
						var h=d[g]('body')[0];
						h.appendChild(s);
					  }})();</script>
					<div class="pluso" data-background="transparent" data-options="big,square,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir"></div>
					
                </div>
                <div class="col-md-4 text-center">
                    <h3>Или создай свою сказку сейчас<br></h3>
                    <a href="." class="btn btn-lg btn-success">Создать за 1 клик!</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /Call to Action -->

	
    <div id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                </div>
            </div>
			 <div class="row starring">
                <div class="col-md-6 text-center">
                    <h2>Что говорят критики</h2>
                    <hr>
                    <!-- Put this script tag to the <head> of your page -->
					<script type="text/javascript" src="//vk.com/js/api/openapi.js?105"></script>

					<script type="text/javascript">
					  VK.init({apiId: 3852004, onlyWidgets: true});
					</script>

					<!-- Put this div tag to the place, where the Comments block will be -->
					<div id="vk_comments"></div>
					<script type="text/javascript">
					VK.Widgets.Comments("vk_comments", {limit: 15, width: "520", attach: "*", pageUrl:"http://skillme.in/tales/"});
					</script>
                </div>
                <div class="col-md-6 text-center">
                    <h2>Другие сказки</h2>
					<?php
					$tales = get_random_tales(5);
					foreach($tales as $tale){
						 print_fairytale($tale);
					}
					?>
                </div>
			</div>
			
        </div>
    </div>
	
    <div  class="callout spoiler" id="spoiler5">
        <div class="vert-text">
			<h3>Ну а кто был репкой, вы узнаете в другой раз</h3>
            <h1>Продолжение следует...</h1>
            <h3>произносит <?php echo $users[0]['first_name']; ?>, занавес опускается, свет гаснет</h3>
        </div>
    </div>
    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <p>Copyright &copy; <a href="http://skillclub.ru">SkillClub</a>, <a href="http://skillme.in	">SkillMe</a></p>
                </div>
            </div>
        </div>
    </footer>
    <!-- /Footer -->

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>

    <!-- Custom JavaScript for the Side Menu and Smooth Scrolling -->
    <script>
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    </script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    </script>
    <script>
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    </script>
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter22194755 = new Ya.Metrika({id:22194755, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/22194755" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
</body>

</html>
